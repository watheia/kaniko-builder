FROM alpine:3.12

COPY --from=gcr.io/kaniko-project/executor:debug /kaniko/executor /kaniko/executor

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates bash

COPY scripts/kaniko-builder.sh /usr/local/bin/kaniko-builder
RUN chmod +x /usr/local/bin/kaniko-builder

ENV PATH="${PATH}:/usr/local/bin"

WORKDIR /workspace
VOLUME /workspace

# Use bash entrypoint
ENTRYPOINT []
CMD ["bash", "-C"]
