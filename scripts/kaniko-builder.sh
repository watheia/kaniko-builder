#!/usr/bin/env bash

set -eu
set -o pipefail

ADDITIONALTAGLIST=""
CI_COMMIT_BRANCH="local"
CI_COMMIT_TAG="v0alpha1"
CI_COMMIT_REF_NAME="latest"
CI_COMMIT_SHA="0000000000000000000000000000000000000000"
CI_COMMIT_SHORT_SHA=""
CI_DEFAULT_BRANCH="master"
CI_JOB_URL='https://gitlab.com/watheia/kaniko-builder/-/jobs/0000000000'
CI_MERGE_REQUEST_ID=""
CI_PIPELINE_URL='https://gitlab.com/watheia/kaniko-builder/-/pipelines/000000000'
CI_PROJECT_TITLE="Materialio"
CI_PROJECT_URL='https://gitlab.com/watheia/kaniko-builder'
CI_REGISTRY_IMAGE="docker.io/watheialabs/kaniko-builder"
CI_SERVER_URL='https://gitlab.com'
GITLAB_USER_EMAIL='admin@watheia.org'
GITLAB_USER_LOGIN='aaron.miller'
VERSIONLABELMETHOD="OnlyIfThisCommitHasVersion"

function print::title() {
    local blue reset message
    blue="\033[0;34m"
    reset="\033[0;39m"
    message="${1}"

    echo -e "\n${blue}${message}${reset}" >&2
}

function print::info() {
    local message
    message="${1}"

    echo -e "${message}" >&2
}

function print::error() {
    local message red reset
    message="${1}"
    red="\033[0;31m"
    reset="\033[0;39m"

    echo -e "${red}${message}${reset}" >&2
    exit 1
}

function print::success() {
    local message green reset
    message="${1}"
    green="\033[0;32m"
    reset="\033[0;39m"

    echo -e "${green}${message}${reset}" >&2
    exitcode="${2:-0}"
    exit "${exitcode}"
}

function print::warn() {
    local message yellow reset
    message="${1}"
    yellow="\033[0;33m"
    reset="\033[0;39m"

    echo -e "${yellow}${message}${reset}" >&2
}

function usage() {
    cat <<-USAGE
kaniko-builder [OPTIONS]

Packages a Dockerfile into an OCI compatible container.

OPTIONS
  --help                        -h prints the command usage
  --destination <destination>   -d <destination>   Full image tage name
  --dockerfile <dockerfile>     -f <version>  specifies Dockerfile path relative to context
  --context <context>           -c <context>   Full image tage name
  --cache                       Enable cache
USAGE
}

function image_labels() {
    cat <<EOF
--label org.opencontainers.image.vendor=$CI_SERVER_URL/$GITLAB_USER_LOGIN
--label org.opencontainers.image.authors=$CI_SERVER_URL/$GITLAB_USER_LOGIN
--label org.opencontainers.image.revision=$CI_COMMIT_SHA
--label org.opencontainers.image.source=$CI_PROJECT_URL
--label org.opencontainers.image.documentation=$CI_PROJECT_URL
--label org.opencontainers.image.licenses=$CI_PROJECT_URL
--label org.opencontainers.image.url=$CI_PROJECT_URL
--label vcs-url=$CI_PROJECT_URL
--label com.gitlab.ci.user=$CI_SERVER_URL/$GITLAB_USER_LOGIN
--label com.gitlab.ci.email=$GITLAB_USER_EMAIL
--label com.gitlab.ci.tagorbranch=$CI_COMMIT_REF_NAME
--label com.gitlab.ci.pipelineurl=$CI_PIPELINE_URL
--label com.gitlab.ci.commiturl=$CI_PROJECT_URL/commit/$CI_COMMIT_SHA
--label com.gitlab.ci.cijoburl=$CI_JOB_URL
--label com.gitlab.ci.mrurl=$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID
EOF
}

function formatted_tag_list() {
    local builddate buildtitle additional_tags versionlabel formatted_tags labels

    #Build date for opencontainers
    labels=$(image_labels)
    builddate="'$(date '+%FT%T%z' | sed -E -n 's/(\+[0-9]{2})([0-9]{2})$/\1:\2/p')'" #rfc 3339 date
    labels="${labels} --label org.opencontainers.image.created=$builddate --label build-date=$builddate"
    #Description for opencontainers
    buildtitle=$(echo "${CI_PROJECT_TITLE}" | tr " " "_")
    additional_tags="$ADDITIONALTAGLIST $CI_COMMIT_REF_NAME $CI_COMMIT_SHORT_SHA"

    labels="$labels --label org.opencontainers.image.title=$buildtitle --label org.opencontainers.image.description=$buildtitle"

    #Add ref.name for opencontainers
    labels="$labels --label org.opencontainers.image.ref.name=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"

    # print::info "labels = ${labels}"

    #Build Version Label and Tag from git tag, LastVersionTagInGit was placed by a previous job artifact
    if [[ "$VERSIONLABELMETHOD" == "LastVersionTagInGit" ]]; then versionlabel=$(cat VERSIONTAG.txt); fi
    if [[ "$VERSIONLABELMETHOD" == "OnlyIfThisCommitHasVersion" ]]; then versionlabel="${CI_COMMIT_TAG}"; fi
    if [[ -n "${versionlabel}" ]]; then
        labels="$labels --label org.opencontainers.image.version=${versionlabel}"
        additional_tags="${additional_tags} ${versionlabel}"
    fi

    formatted_tags=""
    if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then additional_tags="$additional_tags latest"; fi
    if [[ -n "$additional_tags" ]]; then
        for TAG in $additional_tags; do
            formatted_tags="${formatted_tags} --tag $CI_REGISTRY_IMAGE:$TAG "
        done
    fi

    #Reformat Docker tags to kaniko's --destination argument:
    echo "${formatted_tags}" | sed s/\-\-tag/\-\-destination/g
}

# readonly FORMATTED_TAGLIST=$(formatted_tag_list)

function main() {
    local dockerfile destination context cache

    while [[ "${#}" != 0 ]]; do
        case "${1}" in
        --dockerfile | -f)
            dockerfile="${2}"
            shift 2
            ;;

        --destination | -d)
            destination="${2}"
            shift 2
            ;;

        --context | -c)
            context="${2}"
            shift 2
            ;;

        --cache)
            context="true"
            shift 1
            ;;

        --help | -h)
            shift 1
            usage
            exit 0
            ;;

        "")
            # skip if the argument is empty
            shift 1
            ;;

        *)
            print::error "unknown argument \"${1}\""
            ;;
        esac
    done

    if [[ -z "${destination:-}" ]]; then
        usage
        echo
        print::error "--destination is required"
    fi

    if [[ -z "${dockerfile:-}" ]]; then
        dockerfile="Dockerfile"
    fi

    if [[ -z "${cache:-}" ]]; then
        cache="false"
    fi

    if [[ -z "${context:-}" ]]; then
        if [[ -f /kaniko/executor ]]; then
            context="."
        else
            context=$(pwd)
        fi
        print::info "Selected default context: $(realpath "${context}")"
    fi

    local taglist=""
    taglist=$(formatted_tag_list)

    print::title "Building and shipping image to ${taglist}"
    echo

    if [[ -f /kaniko/executor ]]; then
        echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD | base64)\"}}}" >/kaniko/.docker/config.json
        echo -- /kaniko/executor \
            --dockerfile "/workspace/${dockerfile}" \
            --context "dir:///workspace/${context}" \
            --cache="${cache}" ${taglist} $(image_labels) #intentional dequote
    else
        echo "${CI_REGISTRY_PASSWORD}" | docker login --password-stdin --username watheialabs
        docker run \
            -v "$context":/workspace \
            gcr.io/kaniko-project/executor:latest \
            --dockerfile "/workspace/${dockerfile}" \
            --context dir:///workspace/ \
            --cache="${cache}" ${taglist} $(image_labels) #intentional dequote
    fi

    print::success "Kaniko Build Complete."
}

main "${@:-}"

# export VERSIONLABELMETHOD="OnlyIfThisCommitHasVersion"

# dockerfile=$1
# destination=$2

# echo "Building and shipping image to $destination"

# echo "$FORMATTEDTAGLIST"
# echo "$IMAGE_LABELS"
